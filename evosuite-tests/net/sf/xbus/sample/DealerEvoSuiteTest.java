/*
 * This file was automatically generated by EvoSuite
 */

package net.sf.xbus.sample;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import net.sf.xbus.base.core.XException;
import net.sf.xbus.sample.Dealer;
import org.evosuite.runtime.System;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DealerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      String string0 = dealer0.getValue("CB)Yv{BD\"\\bW?gVAI?", (String) null, "CB)Yv{BD\"\\bW?gVAI?");
      assertNull(string0);
  }

  @Test
  public void test1()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      // Undeclared exception!
      try {
        dealer0.replaceMarker("$DEALERNAME$", "$DEALERNAME$");
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      boolean boolean0 = dealer0.hasMarker((String) null);
      assertEquals(false, boolean0);
  }

  @Test
  public void test3()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      boolean boolean0 = dealer0.hasMarker("$DEALERNAME$");
      assertEquals(true, boolean0);
  }

  @Test
  public void test4()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      boolean boolean0 = dealer0.hasMarker("$DEALERNUMBER$");
      assertEquals(true, boolean0);
  }

  @Test
  public void test5()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      boolean boolean0 = dealer0.hasMarker("");
      assertEquals(false, boolean0);
  }

  @Test
  public void test6()  throws Throwable  {
      Dealer dealer0 = new Dealer();
      String string0 = dealer0.replaceMarker("$DEALERNUMBER$", "");
      assertNotNull(string0);
      assertEquals("", string0);
  }
}
