/*
 * This file was automatically generated by EvoSuite
 */

package net.sf.xbus.base.xml;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import net.sf.xbus.base.xml.XParserErrorHandler;
import org.junit.BeforeClass;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

@RunWith(EvoSuiteRunner.class)
public class XParserErrorHandlerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      XParserErrorHandler xParserErrorHandler0 = new XParserErrorHandler();
      // Undeclared exception!
      try {
        xParserErrorHandler0.error((SAXParseException) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      XParserErrorHandler xParserErrorHandler0 = new XParserErrorHandler();
      // Undeclared exception!
      try {
        xParserErrorHandler0.warning((SAXParseException) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      XParserErrorHandler xParserErrorHandler0 = new XParserErrorHandler();
      // Undeclared exception!
      try {
        xParserErrorHandler0.fatalError((SAXParseException) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
