/*
 * This file was automatically generated by EvoSuite
 */

package net.sf.xbus.base.core.trace;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import net.sf.xbus.base.core.XException;
import net.sf.xbus.base.core.trace.Formatter;
import org.evosuite.runtime.System;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class FormatterEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Formatter formatter0 = new Formatter();
      assertNotNull(formatter0);
  }

  @Test
  public void test1()  throws Throwable  {
      // Undeclared exception!
      try {
        Formatter.format("", 0);
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      // Undeclared exception!
      try {
        Formatter.format("", (-130));
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test3()  throws Throwable  {
      // Undeclared exception!
      try {
        Formatter.format("", 2);
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test4()  throws Throwable  {
      // Undeclared exception!
      try {
        Formatter.format(">W0>zn_gM0Asr9!Dc", 3);
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test5()  throws Throwable  {
      // Undeclared exception!
      try {
        Formatter.format("KcbqH+4+7\"9EEJ=cC%H", 4);
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }
}
