/*
 * This file was automatically generated by EvoSuite
 */

package net.sf.xbus.base.core.reflection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import net.sf.xbus.base.core.reflection.XBUSClassLoader;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class XBUSClassLoaderEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      XBUSClassLoader.getInstance((ClassLoader) null);
  }
}
