/*
 * This file was automatically generated by EvoSuite
 */

package net.sf.xbus.protocol.soap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import net.sf.xbus.base.core.XException;
import net.sf.xbus.base.xbussystem.XBUSSystem;
import net.sf.xbus.protocol.soap.SOAPMessage;
import org.apache.axis.message.RPCElement;
import org.apache.axis.message.SOAPDocumentImpl;
import org.evosuite.runtime.System;
import org.junit.BeforeClass;
import org.w3c.dom.Document;

@RunWith(EvoSuiteRunner.class)
public class SOAPMessageEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      SOAPMessage sOAPMessage0 = new SOAPMessage((XBUSSystem) null);
      // Undeclared exception!
      try {
        sOAPMessage0.setErrortext("y");
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      SOAPMessage sOAPMessage0 = new SOAPMessage((XBUSSystem) null);
      sOAPMessage0.setErrortext((String) null);
      assertEquals("TEST_EXECUTION_THREAD_1_1372718923009", sOAPMessage0.getId());
  }

  @Test
  public void test2()  throws Throwable  {
      SOAPMessage sOAPMessage0 = new SOAPMessage("x[(aA$)l$%J", (XBUSSystem) null, "x[(aA$)l$%J");
      sOAPMessage0.synchronizeResponseFields((XBUSSystem) null);
      assertEquals("SOAPMessage", sOAPMessage0.getShortname());
  }

  @Test
  public void test3()  throws Throwable  {
      SOAPMessage sOAPMessage0 = new SOAPMessage("x[(aA$)l$%J", (XBUSSystem) null, "x[(aA$)l$%J");
      Object[] objectArray0 = new Object[1];
      RPCElement rPCElement0 = new RPCElement("x[(aA$)l$%J", "x[(aA$)l$%J", objectArray0);
      SOAPDocumentImpl sOAPDocumentImpl0 = (SOAPDocumentImpl)rPCElement0.getOwnerDocument();
      // Undeclared exception!
      try {
        sOAPMessage0.setResponseDocument((Document) sOAPDocumentImpl0, (XBUSSystem) null);
        fail("Expecting exception: System.SystemExitException");
      } catch(System.SystemExitException e) {
      }
  }
}
